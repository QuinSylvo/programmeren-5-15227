//Map variable
const myMap =L.map('map');

//Load basemap
const myBasemap= L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
  maxZoom: 19,
  attribution: '© <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
});
 //Define Icon
const cafeIcon = L.icon({
   iconUrl:'images/beerMug.png',
    shadowUrl:'',
    iconSize:[25,35]
    
});



//Add basemap to my map id
myBasemap.addTo(myMap);

//Set view of the map

myMap.setView([51.2194475,4.4024643],12)

//Adding one cafe with popUp info binding
/*
const deMuze =L.marker([51.2205, 4.4027],{icon: cafeIcon}).bindPopup(`
<h2>De Muze</h2>
<p><b>Adress:</b> Melkmarkt 15</p>
<p><b>Ambiance:</b>4/5</p>
<p><b>Type:</b> Cafe</p>
<p><b>Comments:</b> Good cappuccino, great live music, nice cozy place</p>
`
).openPopup().addTo(myMap);

const kellyIrishPub=L.marker([51.2177, 4.4184],{icon: cafeIcon}).bindPopup(`
<h2>Kelly's Irish Pub</h2>
<p><b>Adress:</b> De keyserlei 27</p>
<p><b>Ambiance:</b>5/5</p>
<p><b>Type:</b> Pub</p>
<p><b>Comments:</b>"Live Football broadcasts, Good beers, good food</p>
`
).openPopup().addTo(myMap);

const irishTimes =L.marker([51.221134,4.3982927],{icon: cafeIcon}).bindPopup(`
<h2>Irish Times Pub</h2>`
).openPopup().addTo(myMap);

*/



//XMLHTTPRequest naar JSON maken

const request = new XMLHttpRequest();
request.open('GET','data.json',true);

request.onload =function(){
    const data =JSON.parse(this.response);

    //Filter cafes op type
const pubType= data.cafes.filter(cafes=>{});
    
    const Cafe = data.cafes.filter(cafes=>{return cafes.type==='Cafe'});
     const Pub = data.cafes.filter(cafes=>{return cafes.type==='Pub'});
     
    
    const pubTypeArray =[
        {
            name:"Cafe",
            number: Cafe.length,
        },
        {
            name:"Pub",
            number:Pub.length,
        }
    ];
    
    for(let typesOfPub of pubTypeArray){
        console.log(typesOfPub.name, typesOfPub.name);
    }
    
    const cafeTypeCount = data.cafes.reduce((sums,caFe)=>{sums[caFe.type]=(sums[caFe.type]||0)+1; return sums;
    },{});
    
     
     // Create a sidebar
  const sidebar = document.getElementById('buurt');
      const h3 = document.createElement("h3");
  h3.innerHTML = "Types of Cafe";
  sidebar.appendChild(h3);
    
    // print alles in buurt sidebar
       for(let cafePubs in cafeTypeCount){
        const p= document.createElement("p");
           p.innerHTML =`<b>${cafePubs}</b>: ${cafeTypeCount[cafePubs]}`;
           sidebar.appendChild(p);
    }

 

    
    
     // Print cafe markers
    const cafes = data.cafes.map(cafes => {
    L.marker([cafes.lat, cafes.long],{icon: cafeIcon}).bindPopup(`
        <h2>${cafes.name}</h2>
        <p><b>Adress:</b> ${cafes.adress}</p>
        <p><b>Ambiance:</b> ${cafes.ambiance}</p>
        <p><b>Type:</b> ${cafes.type}</p>
        <p><b>Comments:</b> ${cafes.comments}</p>
    `).openPopup().addTo(myMap);
    });
}

request.send();
